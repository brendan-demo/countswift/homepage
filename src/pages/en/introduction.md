---
title: Introduction
description: Docs intro
layout: ../../layouts/MainLayout.astro
---

**Welcome to CountSwift.dev!**

CountSwift is a simple full-stack app that shows the discography of the one and only [Taylor Swift]().

But the app's purpose isn't to talk about Taylor - plenty of more qualified people to discuss that.  This app is designed to be a full-scale app valuable example in several different ways.

## Stack
The stack for this app mimics many simple full-stack apps out there:
- [Node.js]() for serving the app
- [Express]() as HTTP and API handler
- [Vue]() for the frontend
- [Postgres]() for the data layer

For more details, see [Contributing to CountSwift](contributing).

## Production
Production exists in a few areas, mostly to support the [migrating from Heroku's free tier](heroku) project.

- **Heroku Production**: [heroku.countswift.dev](http://heroku.countswift.dev/)

## Development
The code for the app is hosted on [GitLab](). 

## Contributing

- To contribute to the app, see [Contributing to CountSwift](contributing).
- To contribute to these docs, see [Contributing to documentation](contribute-docs).



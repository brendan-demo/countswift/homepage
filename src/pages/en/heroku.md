---
title: Heroku
description: Migrating from Herkou's free tier
layout: ../../layouts/MainLayout.astro
---

## Migrating from Herkou's free tier

Many folks are looking at migrating away from [Heroku]() now that the free tier is being deprecated.  Heroku is how many developers got started with full stack development, and many folks have written fully functional apps running for years on the free tier.  To preserve this proud tradition, on this site, you can see step-by-step instructions for migrating the stack from Heroku to these other providers with similar free tier offerings:

- [Heroku to Fly.io]()
- [Heroku to Render]()
